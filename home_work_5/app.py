from flask import Flask, render_template
import base58


app = Flask(__name__)


@app.route("/")
def index():
    return render_template('index.html')


@app.route('/base58encode/')
def base58encode():
    return '<h1>Кодирование входной строки `STRING` в формат base58</h1>' \
           '<h3>Введите в адресную строку - строку для кодировки!</h3>'


@app.route('/base58encode/<input_string>')
def base58encode_line(input_string):
    line = escape(input_string)
    new_line = escape(base58.b58encode(line))
    return line + new_line


# app.run()
